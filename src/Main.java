public class Main {

    public static void main(String[] args) {
        String[] nodeNames = new String[] {"name1", "name2", "name3"};
        Node head = new Node();
        Node curr = head;
        for (int i=0;i<nodeNames.length;i++) {
            curr.name = nodeNames[i];
            curr.next = new Node();
            if(i<nodeNames.length-1) {
                curr = curr.next;
            }
            else {
                curr.next = head;
            }
        }

        // will infinite loop since last node points to first node
        printList(head);



    }

    public static void printList(Node curr) {

        while(curr.next!=null) {
            System.out.println(curr.name);
            curr = curr.next;
        }
        System.out.println(curr.name);
    }

}
